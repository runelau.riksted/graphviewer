import tkinter as tk
import numpy as np
import expressions
import matplotlib.pyplot as plt
import os
os.chdir(os.path.abspath(os.path.dirname(__file__)))

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk


class GraphEntry(tk.Frame):
    def __init__(self, parent, tk_fig:'TkFig', **kwargs):
        self.parent = parent 
        super().__init__(parent, **kwargs)
        self.pack(side=tk.TOP, anchor='w')
        self.entry = tk.Entry(self)
        
        self.show = tk.IntVar(self, 1)
        self.show_box = tk.Checkbutton(self, variable=self.show, onvalue=1, offvalue=0, command=self.toggle_graph )
        self.show_box.grid(column=0, row=0)
        
        self.entry.grid(column=1, row=0)
        self.expression = None
        self.tk_fig = tk_fig
        self.entry.bind('<Return>', self.plot)
        self.x_button = tk.Button(self, text = '❌', command=self.delete, padx=0, pady=0, relief=tk.FLAT)
        self.x_button.grid(column=5, row=0)
        self.entry.bind('<ISO_Left_Tab>', self.on_tab)
        self.entry.bind('<Tab>', self.on_tab)
        self.last_entry = True
        self.ax = self.tk_fig.ax
        
        self.limits_button = tk.Button(self, text = '>', command=self.toggle_limit_entries, padx=0, pady=0, relief=tk.FLAT)
        self.limits_button.grid(column=4, row=0)
        self.limit_entries = []
        
        self.limits = []
        self.graph = None
        
        self.get = self.entry.get
        
    def toggle_limit_entries(self):
        if self.limit_entries:
            self.limit_entries[0].destroy()
            self.limit_entries[1].destroy()
            self.limit_entries.clear()
            self.limits_button.configure(text='>')
        else:
            self.limit_entries = [tk.Entry(self, width=4), tk.Entry(self, width =4)]
            if self.limits:
                self.limit_entries[0].insert(0,'' if self.limits[0] == -np.inf else self.limits[0])
                self.limit_entries[1].insert(0,'' if self.limits[0] == np.inf else self.limits[1])
            
            self.limit_entries[0].bind('<Return>', self.plot)
            self.limit_entries[1].bind('<Return>', self.plot)
            
            self.limit_entries[0].grid(column = 2, row=0)
            self.limit_entries[1].grid(column = 3, row=0)
            self.limits_button.configure(text='<')
    
    def read_limits(self, *event):
        if not self.limit_entries:
            return 
        #print(f"reading_limits,\n{event}")
        x_min = self.limit_entries[0].get()
        x_max = self.limit_entries[1].get()
        
        if x_min == '' and x_max == '':
            self.limits.clear()
        else:
            self.limits = [-np.inf if x_min == '' else float(x_min),
                            np.inf if x_max == '' else float(x_max)]
        #print(f"limits={self.limits}")       
        
        
    def on_tab(self, event):
    
        if event.keysym == 'Tab':
            direction = 1
        else:
            direction = -1
        
        if direction == 1 and self.tk_fig.graph_entries[-1] is self:
            #prevent rap around to the top entry
            self.tk_fig.graph_entries[0].entry.focus_set()
            return 'break'
        else:
            self.tk_fig.graph_entries[ self.tk_fig.graph_entries.index(self) + direction ].entry.focus_set()
            return 'break'
            
            
    def parse(self):
        
        func_str = self.entry.get()
        
        if not func_str:
            #print(f'Empty function string. {func_str}')
            self.expression = None
            return
        
        if self.last_entry:
            self.tk_fig.graph_entries.append( GraphEntry(self.parent, self.tk_fig) )
            self.last_entry = False
        
        self.expression = expressions.parse(func_str)
        
    def toggle_graph(self):
        #This is evaluated after the value of self.show is toggled
        if self.show.get() == 0:
            if self.graph:
                self.graph.set_data([], [])
                self.tk_fig.canvas.draw()
                
            if self in self.tk_fig.plotted_graph_entries:
                self.tk_fig.plotted_graph_entries.remove(self)
        else:
            self.plot()
    
    def plot(self, *event):
        self.parse()
        self.read_limits()
        
        if self.expression == None:
            if self.graph:
                self.graph.set_data([], [])
                self.tk_fig.canvas.draw()
                
                if self in self.tk_fig.plotted_graph_entries:
                    self.tk_fig.plotted_graph_entries.remove(self)
            return
            
        elif self.show.get() == 1:
            x = self.tk_fig.x_array()
            
            if self.limits:
                if x[0] < self.limits[0] and self.limits[0] < x[-1]:
                    min_i = np.argmax(x >= self.limits[0])
                    
                elif x[0] < self.limits[0] and x[-1] < self.limits[0]:
                    min_i = len(x)
                    
                #elif  self.limits[0] < x[0] and self.limits[0] < x[-1]:
                else:
                    min_i = 0
                
                
                if x[0] < self.limits[1] and self.limits[1] < x[-1]:
                    max_i = np.argmax(x > self.limits[1])
                    
                elif x[0] < self.limits[1] and x[-1] < self.limits[1]:
                    max_i = len(x)
                    
                #elif  self.limits[1] < x[0] and self.limits[1] < x[-1]:
                else:
                    max_i = 0
                    
            else:
                min_i = 0
                max_i = len(x)
                
            y = self.expression( x = x[ min_i:max_i ] )
            
            
            if not isinstance(y, np.ndarray):
                y = np.zeros(x[ min_i:max_i ].size) + y
            
            if self.graph:
                self.graph.set_data(x[ min_i:max_i ], y)
            else:
                self.graph = self.ax.plot(x[ min_i:max_i ],y, scalex=False, scaley=False)[0]
                
            
            
            self.tk_fig.canvas.draw()
            if self not in self.tk_fig.plotted_graph_entries:
                self.tk_fig.plotted_graph_entries.append(self)
            
    def delete(self, *event):
        
        if self.graph:
            self.graph.remove()
            self.tk_fig.canvas.draw()
            del self.graph
            
        if self in self.tk_fig.plotted_graph_entries:
            self.tk_fig.plotted_graph_entries.remove(self)
        
        if len(self.tk_fig.graph_entries)>1:
            self.tk_fig.graph_entries.remove(self)
            
            
                
            self.tk_fig.graph_entries[-1].last_entry = True
            self.destroy()
        
        
        

class TkFig(tk.Frame):
    def __init__(self, parent):
        super().__init__(parent)
        
        
        self.graph_entry_frame = tk.Frame(self)
        self.graph_entry_frame.grid(row=1, column=2, sticky='wnes')
        
        self.fig_frame = tk.Frame(self)
        self.fig_frame.grid(row=1, column=1, sticky='wnes')
        self.columnconfigure(1, weight=1)
        self.rowconfigure(1, weight=1)
        
        self.fig = plt.figure()
        self.ax = self.fig.gca()
        self.canvas = FigureCanvasTkAgg(self.fig, self.fig_frame)
        self.toolbar = NavigationToolbar2Tk(self.canvas, self.fig_frame)
        self.toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        
        
        self.graph_entries = [GraphEntry(self.graph_entry_frame, self)]
        self.plotted_graph_entries = []
        self.graph_entries[-1].entry.focus_set()
        
        self.ax.Legend = []
        self.xlims = self.ax.get_xlim()
        cid = self.canvas.mpl_connect('draw_event', self.on_draw)
        self.after_id = False
        self.ax.set_xlabel("x")
        self.ax.set_ylabel("y")
        self.ax.set_xlim(-10,10)
        self.ax.set_ylim(-10,10)
        
    #smooth scaling            
    #x_proto = 0.5*np.linspace(-1.5, 1.5, 700) + 0.5*np.linspace(-1.5, 1.5, 700)**5
    
    #differentiable scaling 
    x_proto = np.concatenate([  1/(-1.5-np.linspace(-1.3, -0.5, 200))+0.5,
                                np.linspace(-0.5, 0.5, 500), 
                                1/(1.5-np.linspace(0.5, 1.3, 200))-0.5] )
    def x_array(self):
        xlim = self.ax.get_xlim()
        return self.x_proto*(xlim[1]-xlim[0])+ (xlim[1]+xlim[0])/2
        
    
    
    def on_draw(self, event):
        #print("Redrawing?")
        
        if self.xlims == self.ax.get_xlim():
            #print("No")  
            return
        #print("Yes")
        #print(self.ax.get_xlim())
        
        x = self.x_array()
        
        for graph_entry in self.plotted_graph_entries:
        
            if graph_entry.limits:
                
                if x[0] < graph_entry.limits[0] and graph_entry.limits[0] < x[-1]:
                    min_i = np.argmax(x >= graph_entry.limits[0])
                    
                elif x[0] < graph_entry.limits[0] and x[-1] < graph_entry.limits[0]:
                    min_i = len(x)
                    
                #elif  graph_entry.limits[0] < x[0] and graph_entry.limits[0] < x[-1]:
                else:
                    min_i = 0
                
                
                if x[0] < graph_entry.limits[1] and graph_entry.limits[1] < x[-1]:
                    max_i = np.argmax(x > graph_entry.limits[1])
                    
                elif x[0] < graph_entry.limits[1] and x[-1] < graph_entry.limits[1]:
                    max_i = len(x)
                    
                #elif  graph_entry.limits[1] < x[0] and graph_entry.limits[1] < x[-1]:
                else:
                    max_i = 0
                    
                #print(f"indexes = ({min_i}, {max_i})")
            else:
                min_i = 0
                max_i = len(x)
            
            graph_entry.graph.set_data(x[ min_i:max_i ], graph_entry.expression(x=x[ min_i:max_i ]))
                
                    
        
        self.xlims = self.ax.get_xlim()
        #print("Draw event")
        
        if self.after_id:
            self.after_cancel(self.after_id )
        
        self.after_id = self.after(100, self.redraw)
        
    def redraw(self, *event):
        self.after_id = False
        self.canvas.draw()
         	


root = tk.Tk(className='graph_viewer')
root.iconphoto(root._w,tk.PhotoImage(file='logo48.png'))
root.title("GraphViewer")

fig = TkFig(root)
fig.grid(column=1, row=1, sticky="wnes")
root.columnconfigure(1, weight=1)
root.rowconfigure(1, weight=1)


root.mainloop()




