import numpy as np
import types
import re
import json

class Expression():
    
    def __init__(self,  operator:str = None, 
                        operands:list = None, 
                        negative:bool = False, 
                        reciprocal:bool = False, 
                        var:str = None, 
                        value:float = None):
        
        #print(expr_str)
        #print(self.parse(expr_str))
        
        self.operator = operator
        self.operands = operands
        self.negative = negative
        self.reciprocal = reciprocal
        self.functions = None
        self.variables = None
        self.value = value
        self.var = var
        
        
    def check_dependencies(self, overwrite=False):
    
        if overwrite or self.functions == None or self.functions == None:
            if not self.variables:
                self.variables = {}
                
            if not _expr.functions:
                self.functions = {}
                    
            def _check(expr:'Expression'):
                    
                if expr.var and not expr.value:
                    _expr.variables[expr.var] = None
                elif e.operator and expr.operator not in ['+','*','^']:
                    
                    _expr.functions[expr.operator] = None
                    
                    for operand in expr.operands:
                        _check(operand)
            
            _check(self)
            
            
    @staticmethod
    def evaluate(expr:'Expression', *args, **kwargs)->np.array:
        
        if not expr:
            return None
        
        def resolve_names(_expr:'Expression'):
            _expr.check_dependencies()
            
            for var in _expr.variables:
                if var not in ['e', 'pi'] and var not in kwargs:
                    raise ValueError(f"Unknown variable '{var}', please pass a definition as a keyword argument e.g. {var}=5, it may also be equal to an appropriately sized numpy array.")
                
                
            for func in _expr.functions:
                
                if func in kwargs:
                    resolve_names(kwargs[func])
                    expr.functions[func] = lambda *args: _evaluate(kwargs[func], *args)
                elif func =='ln':
                    expr.functions[func] = np.log
                elif hasattr(np, func):
                    expr.functions[func] = getattr(np, func)
                elif hasattr(np.math, func):
                    expr.functions[func] = getattr(np.math, func)
                else:
                    if _expr.functions[func] == None:
                        raise ValueError(f"No function found called '{func}'.")
                    
                    
                    
        def _evaluate(_expr:'Expression')->np.array:
            #print(expr)
            #print(kwargs)
            
            if not _expr:
                return None
            
            if not _expr.value == None:
                res = _expr.value
                
            elif not _expr.var == None:
                if _expr.var in kwargs:
                    res = kwargs[_expr.var]
                    
                elif _expr.var == 'e':
                    res = np.e
                    
                elif _expr.var == 'pi':
                    res = np.pi
                
                else:
                    raise ValueError(f"Unknown variable '{var}', please pass a definition as a keyword argument e.g. {var}=5, it may also be equal to an appropriately sized numpy array.")
                
            # expr is an expression, that needs to be further evaluated
            elif not _expr.operator == None:
                if _expr.operator == '+':
                    
                    res = 0
                    
                    for operand in _expr.operands:
                        
                        res += _evaluate(operand)              
                
                elif _expr.operator == '*':
                    
                    res = 1
                    
                    for operand in _expr.operands:
                        
                        res *= _evaluate(operand)
                    
                        
                elif _expr.operator == '^':
                    
                    res = _evaluate( _expr.operands[-1] )
                    
                    for operand in _expr.operands[-2::-1]:
                        
                        res = _evaluate(operand) ** res
                        
                else:
                    func = _expr.operator
                    if func not in expr.functions:
                        if func in kwargs:
                            resolve_names(kwargs[func])
                            expr.functions[func] = lambda *args: _evaluate(kwargs[func], *args)
                        elif func =='ln':
                            expr.functions[func] = np.log
                        elif hasattr(np, func):
                            expr.functions[func] = getattr(np, func)
                        elif hasattr(np.math, func):
                            expr.functions[func] = getattr(np.math, func)
                        else:
                            if _expr.functions[func] == None:
                                raise ValueError(f"No function found called '{func}'.")
                        
                    res = expr.functions[func]( *[_evaluate( operand) for operand in _expr.operands ] )
            else:
                _expr.__str__(indent=1)
                raise ValueError(f"Not a valid expression.")
                
                
            
            if _expr.reciprocal: 
                res =  1/res
                
            if _expr.negative:
                res = -res
            
            return res
            
        resolve_names(expr)
        return _evaluate(expr)
            
            
    def __call__(self, **kwargs):
        return self.evaluate(self, **kwargs)
    

    @staticmethod
    def parse(expr_str:str)->'Expression':
        '''Parse a string containing a mathematical expression into an Expression object (it will tolerate a float or an int).'''
        
        if isinstance(expr_str, float) or isinstance(expr_str,  int):
            #If the expr_str is a float or an int, we simply package it as an expression object.
            return Expression(value=expr_str)
        
        #print(f'Parsing "{expr_str}"')
        
        #Structure of simple_re: ([        ]*)(  |  |  )([         ]+)(  |  |  )
        simple_re = re.compile(r"([a-zA-Z_']*)(\(|\[|\{)([^()\[\]{}]+)(\)|\]|\})")
        komma_re = re.compile(r"([^\,]+)")
        number_re = re.compile(r"^[.,0-9E]+$")
        equals_re = re.compile(r"(?:^|=)([^=]*)")
        operator_re = { '+':re.compile(r"(?:--)*(-?[^+-]+)"), 
                        '*':re.compile(r"(/?[^/*]+)"), 
                        '^':re.compile(r"(?:^|\^|\*\*)([^^*]+)") }
                        
        simple_expressions = []
        var_lists = []
        function_lists = []
        flip_parenthesis = {'(': ')', '[': ']', '{': '}', ')': '(', ']': '[', '}': '{'}
        
        #flip_parenthesis = { **flip_parenthesis, **{flip_parenthesis[key]: key for key in flip_parenthesis} }
        
        operators = ['+', '*', '^']
        remaining_operators = {None:['+', '*', '^'], '+':[ '*', '^'],'*':['^'],'^':[]}
        
        def parse_simple(expr_str:str, caller = None)->'Expression':
            '''parse a string not containing any parentheses into an Expression object'''
            #print(expr_str)
            #print(caller)
            
            if not expr_str:
                return None
            
            modifiers = {}
            
            
            for operator in operators:
                
                if operator == '*' and expr_str[0] == '-':
                    expr_str = expr_str[1:]
                    modifiers['negative'] = True
                
                if operator == '^' and expr_str[0] == '/':
                    expr_str = expr_str[1:]
                    modifiers['reciprocal'] = True
                
                if operator in remaining_operators[caller]:
                    operands = operator_re[operator].findall(expr_str)
                
                    if len(operands) > 1:
                        #print(terms)
                        return Expression(operator=operator, operands= [parse_simple(operand  , caller = operator) for operand in operands], **modifiers)
                        
                        
            
            
            if re.match(r"^#\d+#$", expr_str):
                s_expr =  simple_expressions[int(expr_str[1:-1])]
                if modifiers.get('negative', False):
                    s_expr.negative = True
                if modifiers.get('reciprocal', False):
                    s_expr.reciprocal = True
                return s_expr
            
            try:
                return Expression(var= expr_str, value=float(expr_str), **modifiers)
            except ValueError:
                pass
            
            var_lists[-1][expr_str] = None
            
            return Expression(var=expr_str, **modifiers ) 
            
            
        def _parse(expr_str:str)->'Expression':
            
            for p, q in {('(',')'), ('[',']'), ('{','}')}:
                if not expr_str.count(p) == expr_str.count(q):
                    raise ValueError(f"There are are not an equal amount of '{p}' and '{q}' in the expression")
            #create list of the simple strings not containing any parentheses
            var_lists.append({})
            function_lists.append({})
            
            Done = False
            while not Done:
                Done = True
                for simple in simple_re.finditer(expr_str):
                
                    Done = False
                    
                    #print(simple)
                    #print(f"0:  {simple.group(0)}\n1:  {simple.group(1)}\n2:  {simple.group(2)}\n3:  {simple.group(3)}\n3:  {simple.group(2)}")
                    if simple.group(1):
                        operands = [parse_simple(opperand) for opperand in komma_re.findall( simple.group(3) )]
                        #print(f"komme_re of {simple.group(3)}:")
                        #print(komma_re.findall(simple.group(3)))
                        simple_expressions.append( 
                                                    Expression( operator = simple.group(1), 
                                                                operands = operands) )
                        function_lists[-1][simple.group(1)] = None
                    else:
                        simple_expressions.append( parse_simple(simple.group(3)) )
                    
                    if not flip_parenthesis[simple.group(2)] == simple.group(4):
                        raise ValueError(f"Mismatched parentheses '{simple.group(2)}' doesn't match '{simple.group(4)}' in '{simple.group(1)}{simple.group(2)}{simple.group(3)}{simple.group(4)}'." )
                    
                    expr_str = expr_str.replace( simple.group(0), f"#{len(simple_expressions)-1}#" )
                    #print(*simple_expressions, sep="\n")
                    #print(expr_str)
                    
            expr = parse_simple(expr_str)
            expr.functions = function_lists[-1]
            expr.variables = var_lists[-1]
            
            return expr
                
            
        expr_str = expr_str.replace(' ', '').replace('**', '^')
        #print(expr_str)
        
        sub_exprs = equals_re.findall(expr_str)
        
        if len(sub_exprs)>1:
            return [_parse(sub_expr) for sub_expr in sub_exprs]
        else:
            return _parse(expr_str)
    
    
    priority = {',': 0, '+':1, '-':1, '*':2, '/':2, '^':3, None:4}
        
    def __str__(self, indent:int = None, deapth:int = 0, parent_priority = 0, seperator='')->str:
        
        
        if self.reciprocal:
            if seperator == '*':
                seperator = '/'
            else:
                seperator += '/'
            
        if self.negative:
            # If the expression is reciprocal, we already changed '+' to '+/'
            if seperator[0] == '+':
                seperator = '-' + seperator[1:]
            else:
                seperator += '-'
        
        
        if indent != None:
            self_str = f"\n{' '*(deapth*indent)}{seperator}"
        else:
            self_str = seperator
        
        
        if self.operator:
            priority = self.priority.get(self.operator, 0)
            is_function = (priority == 0)
            
            
            if is_function:
                self_str += f"{self.operator}("
            elif parent_priority >= priority:
                self_str += "("
            
            self_str += self.operands[0].__str__(deapth=deapth+1, indent=indent, parent_priority = priority, seperator= '')
            
            for operand in self.operands[1:]:
                
                
                self_str += operand.__str__(deapth=deapth+1, indent=indent, parent_priority = priority, seperator=',' if is_function else self.operator)
            
            if parent_priority >= priority:
                self_str += ')'
            
        elif self.var:
            self_str += self.var
            
        elif self.value:
            self_str += self.value
    
        return self_str
    
    def pprint(self, indent:int=2):
        print(self.__str__(indent=indent))
        
    def __add__(self, expr2):
        if isinstance(expr2, int) or isinstance(expr2, float) or isinstance(str, float):
            expr2 = Expression(self, expr2)
            
        return Expression(operator = '+', operands = [self, expr2])
    
    
    def __mult__(self, expr2):
        if isinstance(expr2, int) or isinstance(expr2, float) or isinstance(str, float):
            expr2 = Expression(self, expr2)
            
        return Expression(operator = '*', operands = [self, expr2])
            
            

def parse(*args, **kwargs):
    return Expression.parse(*args, **kwargs)


if __name__ == '__main__':
    test_str = "x-/(3+x)*(3/log(x+1)+1)+ 5*( ( 5+1) * ( 3+ 5*(x+1)))"
    #test_str = '5*(3+a)'
    print(test_str)
    
    x = np.linspace(0,2, 10000)
    sin = np.sin

    import time

    Dt = time.time()

    func = Expression.parse(test_str)
    func.pprint()
    Dt = time.time() - Dt

    print(Dt)
    print(func)

    Dt = time.time()

    y = func(x = x )

    Dt = time.time() - Dt

    print(Dt)


    Dt = time.time()

    z = eval("x-1/(3+x)*(3/(x+1)+1)+ 5*( ( 5+1) * ( 3+ 5*(x+1)))")

    Dt = time.time() - Dt

    print(Dt)

    print("raw:")

    Dt = time.time()

    x-1/(3+x)*(3/(x+1)+1)+ 5*( ( 5+1) * ( 3+ 5*(x+1)))

    Dt = time.time() - Dt

    print(Dt)

    print( y -z)







