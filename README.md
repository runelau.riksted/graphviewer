# GraphViewer

This is a simple graph viewer for displaying graphs of analytical functions using matplotlib. The added functionality consists of
1. being able to enter mathematical expressions, which are then plotted,
1. every time the view is changed the graphs will be recalculated to display the graphs for all the values of x which are now visible.

![alt text](GraphViewer_sample.png "The different expressions being plotted with GraphViewer.")

Here is a sample picture of the application. Note that mathematical expressions are only parsed after pressing ENTER.
One can impose limits for the x-values at which the graph is displayed by clicking '>' next to the graphs formula, and then entering the lower and upper bounds in the appearing fields.

# How to setup
Either make a Git clone of the repository or simply download the project folder and unpack it.
The only non standard Python package needed is numpy and matplotlib.
you can install these by typing "pip3 install -r requirements.txt" or "pip install -r requirements.txt" into the console when in the project folder.

# How to run
The application is run by running "graph_viewer.py" with the Python 3 interpreter. (i.e. typing "python graph_viewer.py"  or "python3 graph_viewer.py" into a console after navigating to the GraphViewer folder.

# Requirements
The project requires Python 3 and the the following Python packages:
1. Numpy
1. Matplotlib
1. \[and their dependencies\]

You can install these by typing "pip3 install -r requirements.txt" or "pip install -r requirements.txt" into the console when in the project folder.




